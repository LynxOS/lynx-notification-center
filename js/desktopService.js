const dbus = require('dbus-native');
const sessionBus = dbus.sessionBus();
const desktopServiceName = 'ar.net.lynx.os.desktop.service';
const desktopInterfaceName = desktopServiceName;
const desktopObjectPath = `/${desktopServiceName.replace(/\./g, '/')}`;

desktopServicePrev = sessionBus.getService(desktopServiceName)

async function getdbusIface(){
  returner = await desktopServicePrev.getInterface(desktopObjectPath, desktopInterfaceName, (err, iface) => {
    if (err) {
      console.error(
        `Failed to request interface '${desktopInterfaceName}' at '${desktopObjectPath}' : ${
          err
        }`
          ? err
          : '(no error)'
      );
      process.exit(1);
    }
    return iface;
  });

  return returner
}

async function getNotifications(){
  dbusIface = await desktopServicePrev.getInterface(desktopObjectPath, desktopInterfaceName, (err, iface) => {
    if (err) {
      console.error(
        `Failed to request interface '${desktopInterfaceName}' at '${desktopObjectPath}' : ${
          err
        }`
          ? err
          : '(no error)'
      );
      process.exit(1);
    }
    iface.getNotifications((err,data)=>{
      console.log(err);
      console.log(data);
    });
  });

  return (dbusIface)
}